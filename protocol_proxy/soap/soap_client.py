from __future__ import annotations

import xmltodict
from zeep import Client, Plugin, Transport
from zeep.helpers import serialize_object
from zeep.loader import load_external
from requests import Session
from requests.auth import HTTPBasicAuth
from models.logger import get_logger
from models.signature_schema.segnatura import Segnatura
from models.root_element import Fascicolo
from settings import DIR_SOAP, DEBUG

log = get_logger()

xsd_schema_segnatura = f"{DIR_SOAP}/segnatura_agspr.xsd"
xsd_schema_inserimento = f"{DIR_SOAP}/inserisciDocumentoInFascicolo_agspr.xsd"
xsd_schema_create_fascicolo = f"{DIR_SOAP}/creaFascicolo_agspr.xsd"

error_codes = {
    -1: "Errore nel codice del Web Services.",
    -2: "DST non valido. Il token DST non è corretto o potrebbe essere scaduto.",
    -3: "Errore in fase di login: il file XML di login non è ben formato.",
    -4: "Errore in fase di login: UserId non corretta.",
    -5: "Errore in fase di login: Password non corretta.",
    -6: "Errore in fase di login: Database non raggiungibile.",
    -7: "Errore in fase di login: Sistema in Emergenza.",
    -50: "Errore in fase di importazione del documento.",
    -51: "Errore in fase di importazione del documento: Verifica della firma fallita.",
    -52: "Errore in fase di importazione del documento: allegato mancante.",
    -100: "Errore in fase di protocollazione.",
    -101: "Errore in fase di protocollazione: Fascicolo inesistente.",
    -102: "Errore in fase di protocollazione: Documento inesistente.",
    -103: "Errore in fase di protocollazione: Titolario chiuso.",
    -104: "Errore in fase di protocollazione: Titolario inesistente.",
    -105: "Errore in fase di protocollazione: Titolario non terminale.",
    -106: "Errore in fase di protocollazione: Compilare l'oggetto.",
    -107: "Errore in fase di protocollazione: Inserire un documento principale.",
    -108: "Errore in fase di protocollazione: Referente inesistente.",
    -109: "Errore in fase di protocollazione: Compilare il referente.",
    -110: "L'xml passato non è valido secondo lo schema definito.",
    -111: "Errore in fase di protocollazione: tipo documento inesistente.",
    -120: "Non è stato possibile salvare l'allegato.",
    -130: "Non è stato possibile salvare il file principale.",
    -140: "Non è stato possibile accedere al protocollo.",
}


class SoapPlugin(Plugin):
    """
    Plugin di Zeep per memorizzare la richiesta e la risposta SOAP su log
    (Probabilmente su file di testo, da vedere cosa conviene fare....)
    """

    def ingress(self, envelope, http_headers, operation):
        return envelope, http_headers

    def egress(self, envelope, http_headers, operation, binding_options):
        return envelope, http_headers


class SoapClient(object):
    def __init__(
        self, ws_url_protocol: str, ws_url_fascicolo: str, configuration: dict
    ):
        session = Session()
        session.auth = HTTPBasicAuth(
            username=configuration["username"], password=configuration["password"]
        )

        transport = Transport(
            session=session,
        )
        self._client_doc_area_proto = Client(
            f"{ws_url_protocol}?wsdl",
            plugins=[SoapPlugin()],
            transport=transport,
        )
        self._client_extended_protocol = Client(
            f"{ws_url_fascicolo}?wsdl",
            plugins=[SoapPlugin()],
            transport=transport,
        )

        self.service_doc_area_proto = self._client_doc_area_proto.create_service(
            self._extract_binding(self._client_doc_area_proto),
            ws_url_protocol,
        )
        self.load_schema_xsd(self._client_doc_area_proto, xsd_schema_segnatura)

        self.service_protocollo_extended = (
            self._client_extended_protocol.create_service(
                self._extract_binding(self._client_extended_protocol),
                ws_url_fascicolo,
            )
        )
        self.load_schema_xsd(self._client_extended_protocol, xsd_schema_inserimento)
        self.load_schema_xsd(
            self._client_extended_protocol, xsd_schema_create_fascicolo
        )

    def _extract_binding(self, client: Client):
        # ritorna il primo binding trovato
        binding_list = list(client.wsdl.bindings.values())
        # Estrarre il nome del binding dal primo (e unico) elemento
        return binding_list[0].name.text

    def load_schema_xsd(self, client, xsd_schema):
        log.debug(f"Loading schema xsd: {xsd_schema}")
        schema_doc = load_external(open(xsd_schema, "rb"), None)
        doc = client.wsdl.types.create_new_document(schema_doc, f"file://{DIR_SOAP}")
        doc.resolve()

    def inserimento(self, user: str, token: str, attachment: bytes) -> str:
        """
        Metodo Inserimento
        Inserisce un nuovo protocollo.
        Argomenti in Input:
          strUserName (str): Username
          strDST (str): token di autenticazione della login
          strAttachment (bytes): Attachment
        Dati in Output InserimentoResponse:
          response (str): "Success" se il protocollo è stato inserito correttamente, in caso contrario sarà restituito un oggetto soap-fault

        """
        response = self.service_doc_area_proto.inserimento(
            strUserName=user, strDST=token, strAttachment=attachment
        )
        log.debug(f"Response call:{response}")
        return response

    def protocollazione(self, user: str, token: str, protocollo: Segnatura) -> dict:
        """
        Metodo protocollazione
        Inserisce un nuovo protocollo.
        Argomenti in Input:
          strUserName (str): Username
          strDST (str): token di autenticazione della login
          strAttachment (Segnatura): Ogetto xml Segnatura
        Dati in Output protocollazioneResponse:
          response (str): "Success" se il protocollo è stato inserito correttamente, in caso contrario sarà restituito un oggetto soap-fault

        """
        # Convertire l'oggetto in un dizionario
        try:
            xml = xmltodict.unparse(protocollo.to_dict())
            response = self.service_doc_area_proto.protocollazione(
                strUserName=user, strDST=token, strAttachment=xml.encode("utf-8")
            )
            log.debug(f"Response call:{response}")
            response = dict(serialize_object(response))
            if response["lngErrNumber"] < 0:
                raise Exception(
                    f'{error_codes[response["lngErrNumber"]]} {response["strErrString"]}'
                )
            return response
        except Exception as e:
            log.error(f"Errore protocollazione: {e}", exc_info=DEBUG)
            raise e

    def login(self, cod_ente: str, user: str, password: str) -> str:
        """
        Metodo login
        Effettua il login.
        Argomenti in Input:
          strCodEnte (str): Codice ente
          strUserName (str): Username
          strPassword (str): Password
        Dati in Output loginResponse:
          response (str): "Success" se il login è avvenuto correttamente, in caso contrario sarà restituito un oggetto soap-fault
        """
        response = self.service_doc_area_proto.login(
            strCodEnte=cod_ente, strUserName=user, strPassword=password
        )
        log.debug(f"Response call:{response}")
        return response

    def crea_fascicolo(self, user: str, token: str, fasciolo: Fascicolo) -> dict:
        """
        Metodo creaFascicolo
        Inserisce un nuovo protocollo.
        Argomenti in Input:
          user (str): Username
          token (str): token di autenticazione della login
          fasciolo (Fascicolo): Oggetto fascicolo
        Dati in Output creaFascicoloResponse:
          response (str): "Success" se il protocollo è stato inserito correttamente, in caso contrario sarà restituito un oggetto soap-fault
        """
        xml = xmltodict.unparse(fasciolo.to_dict())
        response = self.service_protocollo_extended.creaFascicolo(
            user=user, DST=token, xml=xml
        )
        response = xmltodict.parse(response)
        log.debug(f"Response call:{response}")
        return response

    def insert_document_in_fascicolo(
        self, user: str, token: str, attachment: str
    ) -> dict:
        """
        Metodo insertDocumentInFascicolo
        Inserisce un nuovo documento nel fascicolo.
        Argomenti in Input:
          user (str): Username
          token (str): token di autenticazione della login
          attachment (str): Attachment
        Dati in Output creaFascicoloResponse:
          response (str): "Success" se il documento è stato inserito correttamente, in caso contrario sarà restituito un oggetto soap-fault
        """
        response = self.service_protocollo_extended.InserisciDocumentoInFascicolo(
            user=user, DST=token, xml=attachment
        )
        response = xmltodict.parse(response)
        log.debug(f"Response call:{response}")
        return response
