from __future__ import annotations
import base64
import copy
from logging import DEBUG
from models.auth_token import AuthClient
from models.signature_schema.descrizione import Descrizione, Documento
from models.excepitions import (
    MainDocumentException,
    ProtocolloException,
)
from models.types import FlowState
from soap.soap_client import SoapClient
from models.logger import get_logger
from typing import List, Tuple

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


def _get_attachment(
    document: dict, client: SoapClient, token: str, configuration: dict, tenant_id: str
) -> Documento:
    try:
        file_base64 = _download_and_encode_pdf(document["url"], tenant_id)
        if file_base64 is None:
            return None
        document_insert = client.inserimento(
            configuration["username"], token, file_base64
        )
        if configuration["tipo_documento"] == "":
            attachment = Documento(
                str(document_insert["lngDocID"]),
                document["name"],
                document["description"],
            )
        else:
            attachment = Documento(
                str(document_insert["lngDocID"]),
                document["name"],
                document["description"],
                configuration["tipo_documento"],
            )
        log.debug(f"Creazione Allegato:{attachment}")
        return attachment
    except Exception as e:
        log.error(f"Errore durante il caricamento dell'allegato: {e}", exc_info=DEBUG)
        return None


def _download_and_encode_pdf(url: str, tenant_id: str) -> str:
    auth_client = AuthClient(tenant_id)
    # Effettua una richiesta GET per scaricare il contenuto del PDF dalla URL
    log.debug(f"Richiesta GET per scaricare il contenuto del PDF dalla URL: {url}")
    response = auth_client.download_file_with_auth(url)
    # Verifica se la richiesta è stata eseguita con successo (codice 200)
    if response.status_code == 200:
        # Ottieni il contenuto del PDF
        pdf_content = response.content
        # Converti il contenuto del PDF in base64
        pdf_base64 = base64.b64encode(pdf_content).decode("utf-8")
        return pdf_base64
    else:
        # Se la richiesta non è andata a buon fine, stampa un messaggio di errore
        log.error(
            f"Errore nella richiesta. Codice: {response.status_code}", exc_info=DEBUG
        )
        return None


def get_descrizione(
    message: dict,
    client: SoapClient,
    token: str,
    configuration: dict,
    flow_message: FlowState,
):
    """
    Costruisce la descrizione del documento.
    """
    log.debug("Creazione Descrizione")
    try:
        main_document, attachments_processed, attachments_not_processed = (
            _process_attachments_list(
                message, client, token, configuration, flow_message
            )
        )
        if not main_document:
            raise MainDocumentException()
        if message["attachments"] and attachments_not_processed:
            raise ProtocolloException()
        return Descrizione(main_document, attachments_processed)
    except MainDocumentException as mde:
        log.error(
            f"Errore durante la creazione del documento principale: {mde}",
            exc_info=DEBUG,
        )
        raise mde
    except ProtocolloException as pe:
        log.error(f"Errore durante la creazione degli allegati: {pe}", exc_info=DEBUG)
        return handle_retry_exception(main_document, attachments_processed)
    except Exception as e:
        log.error(f"Errore durante la creazione della descrizione: {e}", exc_info=DEBUG)
        if main_document:
            handle_retry_exception(main_document, attachments_processed)
        else:
            raise MainDocumentException()


def handle_retry_exception(
    main_document: Documento, attachments_processed: List[Documento]
) -> dict:
    """
    Gestisce l'eccezione durante la creazione della descrizione.
    """
    attachments_processed_list: List[dict] = []
    for attachment in attachments_processed:
        attachments_processed_list.append(attachment.to_dict())
    return {
        "main_document": main_document.to_dict(),
        "attachments": attachments_processed_list,
    }


def _process_attachments_list(
    message: dict,
    client: SoapClient,
    token: str,
    configuration: dict,
    flow_message: FlowState,
) -> Tuple[
    Documento,
    List[Documento],
    List[dict],
]:
    attachments_processed: List[Documento] = []
    attachments_not_processed = []
    if flow_message == FlowState.PARTIAL_REGISTRATION:
        main_document = Documento.from_dict(message["retry_meta"]["main_document"])
        if message["attachments"]:
            attachments_processed, attachments_not_processed = _retry_attchments(
                message
            )
    else:
        main_document = _get_attachment(
            message["main_document"],
            client,
            token,
            configuration,
            message["tenant_id"],
        )
        if message["attachments"]:
            attachments_not_processed = copy.deepcopy(message["attachments"])

    for attachment in attachments_not_processed[:]:
        document: Documento = _get_attachment(
            attachment, client, token, configuration, message["tenant_id"]
        )
        if not document:
            raise ProtocolloException()
        attachments_processed.append(document)
        attachments_not_processed.remove(attachment)
    return main_document, attachments_processed, attachments_not_processed


def _retry_attchments(message: dict) -> Tuple[List[Documento], List[dict]]:
    """
    Gestisce il retry degli allegati.
    """
    log.debug("Retry allegati")
    processed_attachments = message["retry_meta"]["attachments"]
    not_processed_attachments: List[dict] = copy.deepcopy(message["attachments"])
    documents: List[Documento] = []
    for attachment in processed_attachments:
        documento: Documento = Documento.from_dict(attachment)
        documents.append(documento)
        attachment_processed = _check_attachements_in_list(
            documento.nome, not_processed_attachments
        )
        if attachment_processed:
            not_processed_attachments.remove(attachment_processed)
    return documents, not_processed_attachments


def _check_attachements_in_list(name: str, list: List[dict]):
    """
    Controlla se un allegato è presente nella lista.
    """
    for attachment in list:
        if attachment["name"] == name:
            return attachment
        else:
            return None
