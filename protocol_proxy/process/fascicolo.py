from __future__ import annotations
from datetime import datetime
from models.logger import get_logger
from configuration.utils import get_storage_fascicoli, save_storage_fascicolo
from models.root_element import Fascicolo
from models.excepitions import FascioloNotCreated
from soap.soap_client import SoapClient
from settings import DEBUG


log = get_logger()


def get_configuration_fascicolo(
    configuration: dict, client: SoapClient, token: str, remote_id: str, subject: str
) -> dict:
    try:
        conf = {
            "class_cod": configuration["class_cod"],
            "ID": remote_id,
            "subject": subject,
        }
        check_conf: bool = (
            not configuration["folder_number"] and not configuration["folder_year"]
        )
        log.debug(f"Check conf: {check_conf}")
        log.debug(f"Configuration: {configuration}")
        if not check_conf:
            conf["@anno"] = configuration["folder_year"] or ""
            conf["@numero"] = configuration["folder_number"] or ""
        else:
            fascicoli = {}
            fascicoli = get_storage_fascicoli()
            log.debug(f"Fascicoli: {fascicoli}")
            if fascicoli and remote_id in fascicoli:
                fascicolo = fascicoli[remote_id]
                conf["@anno"] = fascicolo["@anno"]
                conf["@numero"] = fascicolo["@numero"]
            else:
                conf = create_fascicolo(configuration, client, token, subject, conf)
                log.debug(f"Fascicolo creato: {conf}")
                fascicoli[remote_id] = {
                    "@anno": conf["@anno"],
                    "@numero": conf["@numero"],
                }
                save_storage_fascicolo(fascicoli)
        return conf
    except Exception as e:
        log.error(f"errore di configurazione per fascicoli: {e}", exc_info=DEBUG)
        raise FascioloNotCreated()


def create_fascicolo(
    configuration: dict, client: SoapClient, token: str, subject: str, conf: dict
) -> dict:

    new_fascicolo = Fascicolo(
        configuration["class_cod"],
        str(datetime.now().year),
        subject,
        configuration["competence_unit"],
        configuration["creation_unit"],
        "",
    )
    response = client.crea_fascicolo(configuration["username"], token, new_fascicolo)
    if response["ROOT"]["RESULT"] == "OK":
        conf["@anno"] = response["ROOT"]["FASCICOLO_ANNO"]
        conf["@numero"] = response["ROOT"]["FASCICOLO_NUMERO"]
    else:
        log.error(f"Errore creazione fascicolo: {response}", exc_info=DEBUG)
        raise FascioloNotCreated
    return conf
