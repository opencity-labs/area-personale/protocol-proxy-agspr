from __future__ import annotations
import datetime
from soap.soap_client import SoapClient
from models.signature_schema.applicativo_protocollo import (
    ApplicativoProtocollo,
    Parametro,
)
from process.intestazione import get_intestazione
from process.attachments import get_descrizione
from process.fascicolo import get_configuration_fascicolo
from storage.storage_manager import StorageManager
from models.excepitions import (
    FascioloNotCreated,
    LoginException,
    MainDocumentException,
    ProtocolloException,
)
from models.signature_schema.descrizione import Descrizione
from models.signature_schema.segnatura import Segnatura
from models.types import FlowState, Parameter, TrasmissionType
from models.logger import get_logger
from settings import DEBUG

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()

storage_manager = StorageManager()


def process_message_protocollo(
    message: dict,
    configuration: dict,
    client: SoapClient,
    flow_message: FlowState,
):
    try:
        # login sul proxy
        log.debug(f"Login sul proxy: {configuration}")
        # login sul proxy
        token = client.login(
            configuration["codente"],
            configuration["username"],
            configuration["password"],
        )
        if not token["strDST"]:
            log.error(f"Errore login: {token}", exc_info=DEBUG)
            raise LoginException()
        token = token["strDST"]

        # protocollo il messaggio
        remote_id = message["remote_id"]
        subject = message["folder"]["title"]
        fascicolo_configuration = get_configuration_fascicolo(
            configuration,
            client,
            token,
            remote_id,
            subject,
        )
        segnatura = create_segnatura(
            message,
            client,
            token,
            configuration,
            fascicolo_configuration,
            flow_message,
        )
        if type(segnatura) is not Segnatura:
            raise ProtocolloException()
        response_segnatura = client.protocollazione(
            configuration["username"], token, segnatura
        )
        log.debug(response_segnatura)
        response_segnatura["n_fascicolo"] = fascicolo_configuration["@numero"]
        return response_segnatura, True
    except MainDocumentException as mde:
        raise mde
    except LoginException as le:
        raise MainDocumentException(f"Errore Login: {le}")
    except FascioloNotCreated as fnc:
        raise MainDocumentException(f"Errore creazione fascicolo: {fnc}")
    except ProtocolloException as pe:
        return segnatura, False
    except Exception as e:
        log.error(f"Errore protocollo: {e}", exc_info=DEBUG)
        raise e


def create_segnatura(
    message: dict,
    client: SoapClient,
    token: str,
    configuration: dict,
    fascicolo: dict,
    flow_message: FlowState,
):
    try:
        log.debug("Creazione segnatura")
        applicativo_protocollo = get_applicativo_protocollo(
            message["registration_data"]["transmission_type"], configuration
        )
        intestazione = get_intestazione(
            message,
            configuration,
            fascicolo,
        )
        descrizione = get_descrizione(
            message, client, token, configuration, flow_message
        )
        if type(descrizione) is not Descrizione:
            raise ProtocolloException()
        segnatura = Segnatura(intestazione, descrizione, applicativo_protocollo)
        log.debug(segnatura)
        return segnatura
    except MainDocumentException as mde:
        raise mde
    except ProtocolloException as pe:
        return descrizione
    except Exception as e:
        log.error(f"Errore creazione segnatura: {e}", exc_info=DEBUG)
        raise MainDocumentException()


def get_applicativo_protocollo(
    transmission_type: str, configuration: dict
) -> ApplicativoProtocollo:
    if transmission_type == TrasmissionType.INBOUND.value:
        parameter_name = Parameter.DATA_ARRIVO.value
    else:
        parameter_name = Parameter.DATA_SPEDIZIONE.value
    data = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
    parametri = []
    parametro_data = Parametro(parameter_name, data)
    parametri.append(parametro_data)
    if "dispatch_type" in configuration and configuration["dispatch_type"]:
        parametro_dispatch_type = Parametro(
            "tipoSmistamento", configuration["dispatch_type"]
        )
        parametri.append(parametro_dispatch_type)
    applicativo = ApplicativoProtocollo("SDCPROTOCOLPROXY", parametri)
    return applicativo
