from __future__ import annotations
from typing import List
from models.signature_schema.intestazione import (
    Classifica,
    Fascicolo,
    Intestazione,
    Identificatore,
)
from models.signature_schema.mittente import (
    Mittente,
    MittenteAmministrazione,
    MittentePersona,
)
from models.signature_schema.destinatario import (
    Destinatario,
    DestinatarioAmministrazione,
    DestinatarioPersona,
)
from models.signature_schema.indirizzo_telematico import (
    IndirizzoTelematico,
)
from models.types import FluxType, TrasmissionType
from models.signature_schema.aoo import AOO
from models.signature_schema.amministrazione import (
    AmministrazioneUO,
    UnitaOrganizzativa,
)
from models.signature_schema.persona import Persona


def get_aoo(configuration: dict) -> AOO:
    return AOO(
        configuration["codice_aoo"],
        configuration["denominazione"],
    )


def get_persona(author: dict) -> Persona:
    nome = author["name"]
    cognome = author["family_name"]
    cf = author["tax_identification_number"]
    if author["email"]:
        indirizzo = IndirizzoTelematico(author["email"])
    else:
        indirizzo = None
    return Persona(
        Nome=nome,
        Cognome=cognome,
        CodiceFiscale=cf,
        IndirizzoTelematico=indirizzo,
    )


def get_amministrazione(configuration: dict):
    indirizzo = IndirizzoTelematico(configuration["email"])
    unita_organizzativa = UnitaOrganizzativa(configuration["id_unit_org"])
    author = AmministrazioneUO(
        configuration["denominazione"],
        configuration["codice_amministrazione"],
        indirizzo,
        unita_organizzativa,
    )
    return author


def get_mittenti(
    message: dict, transmission_type: str, configuration: dict
) -> List[Mittente]:
    mittenti = []
    if transmission_type == TrasmissionType.INBOUND.value:
        for author in message["author"]:
            persona = get_persona(author)
            mittente = MittentePersona(persona)
            mittenti.append(mittente)
    else:
        amministrazione = get_amministrazione(configuration)
        aoo_code = get_aoo(configuration)
        mittente = MittenteAmministrazione(amministrazione, aoo_code)
        mittenti.append(mittente)
    return mittenti


def get_destinatari(
    message: dict, transmission_type: str, configuration: dict
) -> List[Destinatario]:
    destinatari = []
    if transmission_type == TrasmissionType.OUTOUND.value:
        for author in message["author"]:
            persona = get_persona(author)
            indirizzo = IndirizzoTelematico(author["email"])
            destinatario = DestinatarioPersona(persona, indirizzo)
            destinatari.append(destinatario)
    else:
        amministrazione = get_amministrazione(configuration)
        aoo_code = get_aoo(configuration)
        indirizzo = IndirizzoTelematico(configuration["email"])
        destinatario = DestinatarioAmministrazione(amministrazione, aoo_code, indirizzo)
        destinatari.append(destinatario)
    return destinatari


def get_classifica(configuration: dict) -> Classifica:
    return Classifica(
        configuration["codice_amministrazione"],
        configuration["codice_aoo"],
        configuration["class_cod"],
    )


def get_intestazione(
    message: dict,
    configuration: dict,
    fascicolo_configuration: dict,
) -> Intestazione:
    transmission_type = message["registration_data"]["transmission_type"]
    if transmission_type == TrasmissionType.INBOUND.value:
        flusso = FluxType.ENTRATA.value
    else:
        flusso = FluxType.USCITA.value
    blank_field = "0"
    identificatore = Identificatore(
        configuration["codice_amministrazione"],
        configuration["codice_aoo"],
        blank_field,
        blank_field,
        flusso,
    )
    mittenti = get_mittenti(message, transmission_type, configuration)
    destinatari = get_destinatari(message, transmission_type, configuration)
    classifica = get_classifica(configuration)
    fascicolo = Fascicolo(
        fascicolo_configuration["@numero"], fascicolo_configuration["@anno"]
    )
    intestazione = Intestazione(
        message["title"], identificatore, mittenti, destinatari, classifica, fascicolo
    )
    return intestazione
