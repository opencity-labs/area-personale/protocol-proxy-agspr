import datetime
import json
import uuid
from confluent_kafka import Producer
from models.counter_metrics import CounterMetrics
from settings import (
    APP_NAME,
    KAFKA_BOOTSTRAP_SERVERS,
    DEBUG,
    KAFKA_PRODUCER_TOPIC,
    KAFKA_RETRY_TOPIC,
)
from settings import VERSION as app_version
from models.logger import get_logger

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


def delivery_report(err, msg):
    """Callback function called on producing messages"""
    if err is not None:
        log.error("Message delivery failed: {}".format(err), exc_info=DEBUG)
    else:
        log.debug("Message delivered to {} [{}]".format(msg.topic(), msg.partition()))


def produce_message(topic: str, key: str, message: str):
    # Configura il produttore Kafka
    producer_conf = {
        "bootstrap.servers": KAFKA_BOOTSTRAP_SERVERS,  # Utilizza questo se il produttore è in un contenitore Docker
    }

    # Crea un produttore Kafka
    producer = Producer(producer_conf)
    producer.produce(topic, key=key, value=message, callback=delivery_report)
    producer.poll(1)

    log.debug("Message sent to Kafka")


def produce_message_after_consume(
    message: dict, status: bool, counter_metrics: CounterMetrics
):
    if message:
        message["event_id"] = str(uuid.uuid4())
        message["app_id"] = f"{APP_NAME}:{app_version}"
        message["updated_at"] = (
            datetime.datetime.now().replace(microsecond=0).astimezone().isoformat()
        )
        message["event_created_at"] = message["updated_at"]
        json_data = json.dumps(message)
        key = message["remote_collection"]["id"]
        if status:
            counter_metrics.increment_success(message["tenant_id"])
            topic_to_send = KAFKA_PRODUCER_TOPIC
        else:
            counter_metrics.increment_failure(message["tenant_id"])
            topic_to_send = KAFKA_RETRY_TOPIC
            produce_message(KAFKA_PRODUCER_TOPIC, key, json_data)
        produce_message(topic_to_send, key, json_data)
        log.info(f"Message produced in: {topic_to_send} with key: {key}")
    else:
        log.error("No message received", exc_info=DEBUG)
