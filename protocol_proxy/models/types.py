from enum import Enum


class FlowState(Enum):
    DOCUMENT_CREATED = "DOCUMENT_CREATED"
    REGISTRATION_PENDING = "REGISTRATION_PENDING"
    REGISTRATION_COMPLETE = "REGISTRATION_COMPLETE"
    PARTIAL_REGISTRATION = "PARTIAL_REGISTRATION"
    REGISTRATION_FAILED = "REGISTRATION_FAILED"


class ContactType(Enum):
    SMTP = "smtp"
    URI = "uri"


class StorageType(Enum):
    AZURE = "azure"
    LOCAL = "local"
    S3 = "s3"


class TrasmissionType(Enum):
    INBOUND = "Inbound"
    OUTOUND = "Outbound"


class FluxType(Enum):
    ENTRATA = "E"
    USCITA = "U"


class Parameter(Enum):
    DATA_ARRIVO = "DATA_ARRIVO"
    DATA_SPEDIZIONE = "DATA_SPEDIZIONE"

class MethodApi(Enum):
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    DELETE = "DELETE"
    PATCH = "PATCH"
    TRACE = "TRACE"
