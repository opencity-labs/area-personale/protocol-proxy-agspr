from __future__ import annotations

from models.signature_schema.indirizzo_telematico import IndirizzoTelematico
from models.signature_schema.persona import Persona


class UnitaOrganizzativa:
    def __init__(self, id: str):
        self.id = id

    def to_dict(self) -> dict:
        return {"@id": self.id}


class Amministrazione:
    def __init__(
        self,
        Denominazione: str,
        CodiceAmministrazione: str,
        IndirizzoTelematico: IndirizzoTelematico | None = None,
    ):
        self.Denominazione = Denominazione
        self.CodiceAmministrazione = CodiceAmministrazione
        self.IndirizzoTelematico = IndirizzoTelematico

    def to_dict(self) -> dict:
        dict = {
            "Denominazione": self.Denominazione,
            "CodiceAmministrazione": self.CodiceAmministrazione,
        }
        if self.IndirizzoTelematico is not None:
            dict["IndirizzoTelematico"] = self.IndirizzoTelematico.to_dict()
        return dict


class AmministrazioneUO(Amministrazione):
    def __init__(
        self,
        Denominazione: str,
        CodiceAmministrazione: str,
        IndirizzoTelematico: IndirizzoTelematico | None = None,
        UnitaOrganizzativa: UnitaOrganizzativa | None = None,
    ):
        super().__init__(
            Denominazione=Denominazione,
            CodiceAmministrazione=CodiceAmministrazione,
            IndirizzoTelematico=IndirizzoTelematico,
        )
        self.UnitaOrganizzativa = UnitaOrganizzativa

    def to_dict(self) -> dict:
        dict = super().to_dict()
        if self.UnitaOrganizzativa is not None:
            dict["UnitaOrganizzativa"] = self.UnitaOrganizzativa.to_dict()
        return dict


class AmministrazioneFisica(Amministrazione):
    def __init__(
        self,
        Denominazione: str,
        CodiceAmministrazione: str,
        IndirizzoTelematico: IndirizzoTelematico | None = None,
        IndirizzoPostale: str | None = None,
        Telefono: str | None = None,
        Fax: str | None = None,
    ):
        super().__init__(
            Denominazione=Denominazione,
            CodiceAmministrazione=CodiceAmministrazione,
            IndirizzoTelematico=IndirizzoTelematico,
        )
        self.IndirizzoPostale = IndirizzoPostale
        self.Telefono = Telefono
        self.Fax = Fax

    def to_dict(self) -> dict:
        dict = super().to_dict()
        if self.IndirizzoPostale is not None:
            dict["IndirizzoPostale"] = self.IndirizzoPostale
        if self.Telefono is not None:
            dict["Telefono"] = self.Telefono
        if self.Fax is not None:
            dict["Fax"] = self.Fax
        return dict


class AmministrazioneRuolo(AmministrazioneFisica):
    def __init__(
        self,
        Denominazione: str,
        CodiceAmministrazione: str,
        Ruolo: str,
        IndirizzoTelematico: IndirizzoTelematico | None = None,
        IndirizzoPostale: str | None = None,
        Telefono: str | None = None,
        Fax: str | None = None,
    ):
        super().__init__(
            Denominazione=Denominazione,
            CodiceAmministrazione=CodiceAmministrazione,
            IndirizzoTelematico=IndirizzoTelematico,
            IndirizzoPostale=IndirizzoPostale,
            Telefono=Telefono,
            Fax=Fax,
        )
        self.Ruolo = Ruolo

    def to_dict(self) -> dict:
        dict = super().to_dict()
        dict["Ruolo"] = self.Ruolo
        return dict


class AmministrazionePersona(AmministrazioneFisica):
    def __init__(
        self,
        Denominazione: str,
        CodiceAmministrazione: str,
        Persona: Persona,
        IndirizzoTelematico: IndirizzoTelematico | None = None,
        IndirizzoPostale: str | None = None,
        Telefono: str | None = None,
        Fax: str | None = None,
    ):
        super().__init__(
            Denominazione=Denominazione,
            CodiceAmministrazione=CodiceAmministrazione,
            IndirizzoTelematico=IndirizzoTelematico,
            IndirizzoPostale=IndirizzoPostale,
            Telefono=Telefono,
            Fax=Fax,
        )
        self.Persona = Persona

    def to_dict(self) -> dict:
        dict = super().to_dict()
        dict["Persona"] = self.Persona.to_dict()
        return dict
