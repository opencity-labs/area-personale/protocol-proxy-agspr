from __future__ import annotations
from typing import List

from models.signature_schema.mittente import Mittente
from models.signature_schema.destinatario import Destinatario


class Identificatore:
    def __init__(
        self,
        CodiceAmministrazione: str,
        CodiceAOO: str,
        NumeroRegistrazione: str,
        DataRegistrazione: str,
        Flusso: str,
    ):
        self.CodiceAmministrazione = CodiceAmministrazione
        self.CodiceAOO = CodiceAOO
        self.NumeroRegistrazione = NumeroRegistrazione
        self.DataRegistrazione = DataRegistrazione
        self.Flusso = Flusso

    def to_dict(self) -> dict:
        return {
            "CodiceAmministrazione": self.CodiceAmministrazione,
            "CodiceAOO": self.CodiceAOO,
            "NumeroRegistrazione": self.NumeroRegistrazione,
            "DataRegistrazione": self.DataRegistrazione,
            "Flusso": self.Flusso,
        }


class Fascicolo:
    def __init__(self, numero: str, anno: str):
        self.numero = numero
        self.anno = anno

    def to_dict(self) -> dict:
        return {"@numero": self.numero, "@anno": self.anno}


class Classifica:
    def __init__(
        self, CodiceAmministrazione: str, CodiceAOO: str, CodiceTitolario: str
    ):
        self.CodiceAmministrazione = CodiceAmministrazione
        self.CodiceAOO = CodiceAOO
        self.CodiceTitolario = CodiceTitolario

    def to_dict(self) -> dict:
        return {
            "CodiceAmministrazione": self.CodiceAmministrazione,
            "CodiceAOO": self.CodiceAOO,
            "CodiceTitolario": self.CodiceTitolario,
        }


class Intestazione:
    def __init__(
        self,
        Oggetto: str,
        Identificatore: Identificatore,
        Mittente: List[Mittente],
        Destinatario: List[Destinatario],
        Classifica: Classifica | None = None,
        Fascicolo: Fascicolo | None = None,
    ):
        self.Oggetto = Oggetto
        self.Identificatore = Identificatore
        self.Mittente = Mittente
        self.Destinatario = Destinatario
        self.Classifica = Classifica
        self.Fascicolo = Fascicolo

    def to_dict(self) -> dict:
        dict = {
            "Oggetto": self.Oggetto,
            "Identificatore": self.Identificatore.to_dict(),
        }
        if self.Mittente is not None:
            dict["Mittente"] = [mittente.to_dict() for mittente in self.Mittente]
        if self.Destinatario is not None:
            dict["Destinatario"] = [destinatario.to_dict() for destinatario in self.Destinatario]
        if self.Classifica is not None:
            dict["Classifica"] = self.Classifica.to_dict()
        if self.Fascicolo is not None:
            dict["Fascicolo"] = self.Fascicolo.to_dict()
        return dict
