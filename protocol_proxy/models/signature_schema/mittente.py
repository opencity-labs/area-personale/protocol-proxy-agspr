from models.signature_schema.amministrazione import Amministrazione
from models.signature_schema.persona import Persona
from models.signature_schema.aoo import AOO


class Mittente:
    def __init__(
        self,
    ):
        pass

    def to_dict(self):
        return {}


class MittenteAmministrazione(Mittente):
    def __init__(
        self,
        Amministrazione: Amministrazione,
        AOO: AOO,
    ):
        self.Amministrazione = Amministrazione
        self.AOO = AOO

    def to_dict(self):
        dict = super().to_dict()
        dict["Amministrazione"] = self.Amministrazione.to_dict()
        dict["AOO"] = self.AOO.to_dict()
        return dict


class MittentePersona(Mittente):
    def __init__(
        self,
        Persona: Persona,
    ):
        self.Persona = Persona

    def to_dict(self) -> dict:
        dict = super().to_dict()
        dict["Persona"] = self.Persona.to_dict()
        return dict
