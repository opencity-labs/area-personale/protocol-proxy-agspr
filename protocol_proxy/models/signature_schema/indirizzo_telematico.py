from __future__ import annotations

from models.types import ContactType


class IndirizzoTelematico:
    def __init__(
        self,
        value: str,
        tipo: str = ContactType.SMTP.value,
        note: str | None = None,
    ):
        self.value = value
        self.tipo = tipo
        self.note = note

    def to_dict(self) -> dict:
        result_dict = {
            "@tipo": self.tipo,
        }

        if self.note is not None:
            result_dict["@note"] = self.note

        result_dict["value"] = self.value

        return result_dict
