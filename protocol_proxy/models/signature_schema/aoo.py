from __future__ import annotations


class AOO:
    def __init__(self, CodiceAOO: str, Denominazione: str | None = None):
        self.CodiceAOO = CodiceAOO
        self.Denominazione = Denominazione

    def to_dict(self):
        dict = {
            "CodiceAOO": self.CodiceAOO,
        }
        if self.Denominazione is not None:
            dict["Denominazione"] = self.Denominazione
        return dict
