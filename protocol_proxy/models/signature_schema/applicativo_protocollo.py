from __future__ import annotations
from typing import List


class Parametro:
    def __init__(self, nome: str, valore: str):
        self.nome = nome
        self.valore = valore

    def to_dict(self) -> dict:
        return {"@nome": self.nome, "@valore": self.valore}


class ApplicativoProtocollo:
    def __init__(self, nome: str, parametri: List[Parametro] | None = None):
        self.nome = nome
        self.parametri = parametri or []

    def to_dict(self) -> dict:
        dict = {"nome": self.nome}
        if self.parametri is not None:
            dict["Parametro"] = [parametro.to_dict() for parametro in self.parametri]
        return dict
