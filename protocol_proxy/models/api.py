from typing import Optional, Union
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field

ERR_422_SCHEMA = {
    "description": "Validation Error",
    "content": {
        "application/problem+json": {
            "schema": {
                "$ref": "#/components/schemas/ErrorMessage",
            },
        },
    },
}


class Status(BaseModel):
    status: str


class ErrorMessage(BaseModel):
    type: str
    title: str
    status: Optional[int] = Field(..., format="int32")
    detail: Union[str, dict]
    instance: Optional[str]

    def to_dict(self) -> dict:
        return self.dict()


class ConfigurationTenantPut(BaseModel):
    aoo_code: str
    base_url: str
    created_at: str = ""
    description: str
    institution_code: str
    modified_at: str = ""
    slug: str

    def to_dict(self) -> dict:
        return self.dict()


class ConfigurationTenant(ConfigurationTenantPut):
    id: str


class ConfigurationTenantPatch(ConfigurationTenantPut):
    aoo_code: str = ""
    base_url: str = ""
    created_at: str = ""
    description: str = ""
    institution_code: str = ""
    modified_at: str = ""
    slug: str = ""


class ConfigurationServicePut(BaseModel):
    is_active: bool
    ws_url_fascicolo: str
    ws_url_protocol: str
    username: str
    password: str
    codente: str
    codice_amministrazione: str
    email: str
    class_cod: str
    denominazione: str
    id_unit_org: str
    codice_aoo: str
    dispatch_type: str
    tipo_documento: Optional[str] = ""
    folder_year: Optional[str] = ""
    folder_number: Optional[str] = ""
    competence_unit: Optional[str] = ""
    creation_unit: Optional[str] = ""

    def to_dict(self) -> dict:
        return self.dict()


class ConfigurationServicePatch(ConfigurationServicePut):
    is_active: bool = True
    ws_url_fascicolo: str = ""
    ws_url_protocol: str = ""
    username: str = ""
    password: str = ""
    codente: str = ""
    codice_amministrazione: str = ""
    email: str = ""
    class_cod: str = ""
    denominazione: str = ""
    id_unit_org: str = ""
    codice_aoo: str = ""
    dispatch_type: str = ""
    tipo_documento: Optional[str] = ""
    folder_year: Optional[str] = ""
    folder_number: Optional[str] = ""
    competence_unit: Optional[str] = ""
    creation_unit: Optional[str] = ""

class ConfigurationService(ConfigurationServicePut):
    id: str
    tenant_id: str


class JSONResponseCustom(JSONResponse):
    media_type = "application/problem+json"
