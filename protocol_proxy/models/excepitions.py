class ProtocolloException(Exception):
    pass


class FascioloNotCreated(Exception):
    pass


class LoginException(Exception):
    pass


class MainDocumentException(Exception):
    pass


class JsonException(Exception):
    pass


class ExistException(Exception):
    pass


class NotExistingException(Exception):
    pass
