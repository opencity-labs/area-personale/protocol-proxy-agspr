from __future__ import annotations


class Fascicolo:
    def __init__(
        self,
        CLASS_COD: str,
        FASCICOLO_ANNO: str,
        OGGETTO: str,
        UNITA_COMPETENZA: str,
        UNITA_CREAZIONE: str,
        UTENTE: str,
        FASCICOLO_ANNO_PADRE: str | None = None,
        FASCICOLO_NUMERO_PADRE: str | None = None,
        DATA_APERTURA: str | None = None,
        NOTE: str | None = None,
    ):
        self.CLASS_COD = CLASS_COD
        self.FASCICOLO_ANNO = FASCICOLO_ANNO
        self.FASCICOLO_ANNO_PADRE = FASCICOLO_ANNO_PADRE
        self.FASCICOLO_NUMERO_PADRE = FASCICOLO_NUMERO_PADRE
        self.DATA_APERTURA = DATA_APERTURA
        self.OGGETTO = OGGETTO
        self.UNITA_COMPETENZA = UNITA_COMPETENZA
        self.UNITA_CREAZIONE = UNITA_CREAZIONE
        self.NOTE = NOTE
        self.UTENTE = UTENTE

    def to_dict(self) -> dict:
        dict = {
            "ROOT": {
                "CLASS_COD": self.CLASS_COD,
                "FASCICOLO_ANNO": self.FASCICOLO_ANNO,
                "OGGETTO": self.OGGETTO,
                "UNITA_COMPETENZA": self.UNITA_COMPETENZA,
                "UNITA_CREAZIONE": self.UNITA_CREAZIONE,
                "UTENTE": self.UTENTE,
            }
        }
        if self.FASCICOLO_ANNO_PADRE is not None:
            dict["ROOT"]["FASCICOLO_ANNO_PADRE"] = self.FASCICOLO_ANNO_PADRE
        if self.FASCICOLO_NUMERO_PADRE is not None:
            dict["ROOT"]["FASCICOLO_NUMERO_PADRE"] = self.FASCICOLO_NUMERO_PADRE
        if self.DATA_APERTURA is not None:
            dict["ROOT"]["DATA_APERTURA"] = self.DATA_APERTURA
        if self.NOTE is not None:
            dict["ROOT"]["NOTE"] = self.NOTE
        return dict


class ProtocolloGruppo:
    def __init__(self, ANNO: str, NUMERO: str, TIPO_REGISTRO: str):
        self.ANNO = ANNO
        self.NUMERO = NUMERO
        self.TIPO_REGISTRO = TIPO_REGISTRO

    def to_dict(self) -> dict:
        return {
            "ANNO": self.ANNO,
            "NUMERO": self.NUMERO,
            "TIPO_REGISTRO": self.TIPO_REGISTRO,
        }


class DocumentoFascicolo:
    def __init__(
        self,
        CLASS_COD: str,
        FASCICOLO_ANNO: str,
        FASCICOLO_NUMERO: str,
        UTENTE: str,
    ):
        self.CLASS_COD = CLASS_COD
        self.FASCICOLO_ANNO = FASCICOLO_ANNO
        self.FASCICOLO_NUMERO = FASCICOLO_NUMERO
        self.UTENTE = UTENTE

    def to_dict(self) -> dict:
        return {
            "ROOT": {
                "CLASS_COD": self.CLASS_COD,
                "FASCICOLO_ANNO": self.FASCICOLO_ANNO,
                "FASCICOLO_NUMERO": self.FASCICOLO_NUMERO,
                "UTENTE": self.UTENTE,
            }
        }


class DocumentoFascicoloID(DocumentoFascicolo):
    def __init__(
        self,
        CLASS_COD: str,
        FASCICOLO_ANNO: str,
        FASCICOLO_NUMERO: str,
        UTENTE: str,
        ID_DOCUMENTO: str,
    ):
        super().__init__(CLASS_COD, FASCICOLO_ANNO, FASCICOLO_NUMERO, UTENTE)
        self.ID_DOCUMENTO = ID_DOCUMENTO

    def to_dict(self) -> dict:
        dict = super().to_dict()
        dict["ROOT"]["ID_DOCUMENTO"] = self.ID_DOCUMENTO
        return dict


class DocumentoFascicoloProtocollo(DocumentoFascicolo):
    def __init__(
        self,
        CLASS_COD: str,
        FASCICOLO_ANNO: str,
        FASCICOLO_NUMERO: str,
        UTENTE: str,
        PROTOCOLLO_GRUPPO: ProtocolloGruppo,
    ):
        super().__init__(CLASS_COD, FASCICOLO_ANNO, FASCICOLO_NUMERO, UTENTE)
        self.PROTOCOLLO_GRUPPO = PROTOCOLLO_GRUPPO

    def to_dict(self) -> dict:
        dict = super().to_dict()
        dict["ROOT"]["PROTOCOLLO_GRUPPO"] = self.PROTOCOLLO_GRUPPO.to_dict()
        return dict
