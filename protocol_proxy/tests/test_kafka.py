import json
import pytest

from confluent_kafka.admin import AdminClient, NewTopic
from confluent_kafka import Producer, Consumer
from kafka_wrapper.consumer_kafka import process_message_kafka
from models.types import FlowState
from models.logger import get_logger
from settings import STAGE
#from rest.rest_client import RestClient

logging = get_logger()

# Definisci il nome del topic temporaneo per il test
TEMPORARY_TOPIC = "temporary_test_topic"

# Configurazione del broker Kafka
if STAGE == "local":
    KAFKA_BROKER = "localhost:29092"
else:
    KAFKA_BROKER = "kafka:9092"


with open("tests/configuration.json", "r") as file:
    configuration = json.load(file)

#client = RestClient(configuration)
# Carica i messaggi di esempio
with open("tests/example_message_produced_in_kafka_sender.json", "r") as file:
    message_sender = json.load(file)

with open(
    "tests/example_message_produced_in_kafka_receiver.json",
    "r",
) as file:
    message_receiver = json.load(file)


@pytest.fixture(scope="function")
def test_kafka_consumer(request):
    # Crea un consumer per il test con un group.id univoco
    group_id = f"test_group_{request.node.name}"
    consumer_conf = {
        "bootstrap.servers": KAFKA_BROKER,
        "group.id": group_id,
        "auto.offset.reset": "earliest",
    }
    consumer = Consumer(consumer_conf)
    yield consumer
    # Chiudi il consumer dopo l'utilizzo
    consumer.close()


@pytest.fixture(scope="module")
def test_kafka_producer():
    # Crea un producer per il test
    producer_conf = {"bootstrap.servers": KAFKA_BROKER}
    producer = Producer(producer_conf)
    yield producer
    # Chiudi il producer dopo l'utilizzo
    producer.flush()


@pytest.fixture(scope="module")
def test_kafka_admin():
    # Crea un oggetto AdminClient per la gestione dei topic
    admin = AdminClient({"bootstrap.servers": KAFKA_BROKER})
    yield admin
    # Non è necessario chiudere l'oggetto AdminClient


@pytest.fixture(scope="function")
def test_kafka_topic(test_kafka_admin, request):
    # Crea un nome di topic univoco basato sul nome del test
    temporary_topic = f"{TEMPORARY_TOPIC}_{request.node.name}"
    # Crea il topic temporaneo per il test
    new_topic = NewTopic(temporary_topic, num_partitions=1, replication_factor=1)
    test_kafka_admin.create_topics([new_topic])
    yield temporary_topic
    # Cancella il topic dopo l'utilizzo
    test_kafka_admin.delete_topics([temporary_topic])


def test_kafka_message_flow_with_sender_message(
    test_kafka_consumer, test_kafka_producer, test_kafka_topic
):
    logging.info("Sending message to Kafka...")
    test_kafka_producer.produce(
        test_kafka_topic, value=json.dumps(message_sender).encode("utf-8")
    )
    test_kafka_producer.flush()

    logging.info("Consuming message from Kafka...")
    test_kafka_consumer.subscribe([test_kafka_topic])
    try:
        msg = test_kafka_consumer.poll(timeout=20.0)
        if msg is None:
            raise Exception("No message received from Kafka.")
        received_message = msg.value().decode("utf-8")
        logging.debug(f"Received message: {received_message}")

        assert json.loads(received_message) == message_sender
    except Exception as e:
        logging.error(f"Error in message flow: {e}")

    # msg_send, status = process_message_kafka(
    #     message_sender, configuration, client, FlowState.DOCUMENT_CREATED
    # )
    # assert status == True

    # assert (
    #     msg_send["folder"]["id"] == "n/a"
    #     and msg_send["registration_data"]["date"]
    #     and msg_send["registration_data"]["document_number"]
    # )


def test_kafka_message_flow_with_recevier_message(
    test_kafka_consumer, test_kafka_producer, test_kafka_topic
):
    logging.info("Sending message to Kafka...")
    test_kafka_producer.produce(
        test_kafka_topic, value=json.dumps(message_receiver).encode("utf-8")
    )
    test_kafka_producer.flush()

    logging.info("Consuming message from Kafka...")
    test_kafka_consumer.subscribe([test_kafka_topic])
    try:
        msg = test_kafka_consumer.poll(timeout=20.0)
        if msg is None:
            raise Exception("No message received from Kafka.")
        received_message = msg.value().decode("utf-8")
        logging.debug(f"Received message: {received_message}")

        assert json.loads(received_message) == message_receiver
    except Exception as e:
        logging.error(f"Error in message flow: {e}")

    # msg_send, status = process_message_kafka(message_receiver, configuration, client, FlowState.DOCUMENT_CREATED)
    # assert status == True

    # assert (
    #     msg_send["folder"]["id"] == "n/a"
    #     and msg_send["registration_data"]["date"]
    #     and msg_send["registration_data"]["document_number"]
    # )
