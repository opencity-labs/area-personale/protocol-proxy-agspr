FROM tiangolo/uvicorn-gunicorn-fastapi:python3.10

#set timezone
ENV TZ=Europe/Rome

RUN mkdir -p protocol_proxy/tests/log

COPY . /code

# Set the working directory
WORKDIR /code/protocol_proxy

# Copy requirements.txt and install dependencies
COPY ./protocol_proxy/requirements.txt /code/protocol_proxy/requirements.txt
RUN pip install --no-cache-dir -r /code/protocol_proxy/requirements.txt

# Expose the debugging port
EXPOSE 5678

# Start the application with uvicorn and enable ptvsd for debugging
CMD ["python", "-m", "ptvsd", "--host", "0.0.0.0", "--port", "5678", "/usr/local/bin/uvicorn", "main_api:app", "--host", "0.0.0.0", "--port", "8000"]